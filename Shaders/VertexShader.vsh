﻿#version 420

layout (location = 0) in vec3 VertexPosition_ModelSpace;
layout (location = 1) in vec3 VertexNormal;
layout (location = 2) in vec3 VertexColor;

uniform mat4 ModelMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;
uniform mat4 NormalMatrix;

uniform vec3 LightReflectivity;
uniform vec3 LightIntensity;

out vec3 FragmentColor;
out vec3 DiffusedLight;

void main(void)
{
	vec4 Vertex = vec4(VertexPosition_ModelSpace, 1);
	gl_Position = ProjectionMatrix * ViewMatrix * ModelMatrix * Vertex;
	FragmentColor = vec3(Vertex.x, Vertex.y, Vertex.z);
}