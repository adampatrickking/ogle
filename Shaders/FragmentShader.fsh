﻿#version 420

in vec3 FragmentColor;
in vec3 DiffusedLight;

out vec3 color;
 
void main()
{
	color = FragmentColor;
}