﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Ogle.Shaders
{
    public static class ShaderUtils
    {    
        public enum VertexAttributes
        {
            VertexPosition = 0,
            VertexColor = 1,
        };

        public static uint CreateProgramFromFiles(string VertexShaderFile, string FragmentShaderFile)
        {
            // Finalized linked program ID
            uint ShaderProgramID;
            // Create shader types to link
            uint VertexShaderID = (uint)GL.CreateShader(ShaderType.VertexShader);
            uint FragmentShaderID = (uint)GL.CreateShader(ShaderType.FragmentShader);
            // Shader compile status
            string ShaderCompileLog;
            int ShaderCompileStatus;
            int ShaderCompileLogLength;

            VertexShaderFile = "Shaders/" + VertexShaderFile + ".vsh";
            FragmentShaderFile = "Shaders/" + FragmentShaderFile + ".fsh";

            // Read the Vertex Shader code from file
            string VertexShaderCode;
            if (File.Exists(VertexShaderFile))
            {
                using (StreamReader FileReader = new StreamReader(VertexShaderFile))
                {
                    Console.WriteLine("Loading Vertex Shader from file..");
                    VertexShaderCode = FileReader.ReadToEnd();
                }                
            }
            else { throw new FileNotFoundException(); }

            // Read the Fragment Shader code from file
            string FragmentShaderCode;
            if (File.Exists(FragmentShaderFile))
            {
                using (StreamReader FileReader = new StreamReader(FragmentShaderFile))
                {
                    Console.WriteLine("Loading Fragment Shader from file..");
                    FragmentShaderCode = FileReader.ReadToEnd();
                }
            }
            else { throw new FileNotFoundException(); }

            // Compile Vertex Shader
            Console.WriteLine("\nCompiling shader: {0}", VertexShaderFile);
            GL.ShaderSource((int)VertexShaderID, VertexShaderCode);
            GL.CompileShader((int)VertexShaderID);

            // Check for errors in Vertex Shader compile            
            GL.GetShader(VertexShaderID, ShaderParameter.CompileStatus, out ShaderCompileStatus);
            GL.GetShader(VertexShaderID, ShaderParameter.InfoLogLength, out ShaderCompileLogLength);
            GL.GetShaderInfoLog((int)VertexShaderID, out ShaderCompileLog);
            if (ShaderCompileLog != null && ShaderCompileLog.Length > 0) { Console.WriteLine("Shader compile errors in {0}: {1}",VertexShaderFile, ShaderCompileLog); }
            else { Console.WriteLine("{0} successfully compiled.", VertexShaderFile); }
            ShaderCompileLogLength = 0;
            ShaderCompileStatus = 0;
            ShaderCompileLog = null;

            // Compile Fragment Shader
            Console.WriteLine("\nCompiling shader: {0}", FragmentShaderFile);
            GL.ShaderSource((int)FragmentShaderID, FragmentShaderCode);
            GL.CompileShader((int)FragmentShaderID);

            // Check for errors in Fragment Shader compile
            GL.GetShader(FragmentShaderID, ShaderParameter.CompileStatus, out ShaderCompileStatus);
            GL.GetShader(FragmentShaderID, ShaderParameter.InfoLogLength, out ShaderCompileLogLength);
            GL.GetShaderInfoLog((int)FragmentShaderID, out ShaderCompileLog);
            if (ShaderCompileLog != null && ShaderCompileLog.Length > 0) { Console.WriteLine("Shader compile errors in {0}: {1}",FragmentShaderFile, ShaderCompileLog); }
            else { Console.WriteLine("{0} successfully compiled", FragmentShaderFile); }
            ShaderCompileLogLength = 0;
            ShaderCompileStatus = 0;
            ShaderCompileLog = null;

            // Link the shader programs into a shader program
            Console.WriteLine("\nLinking shader programs..");
            ShaderProgramID = (uint)GL.CreateProgram();
            GL.AttachShader(ShaderProgramID, VertexShaderID);
            GL.AttachShader(ShaderProgramID, FragmentShaderID);
            GL.LinkProgram(ShaderProgramID);

            // Check the shader program
            GL.GetProgram(ShaderProgramID, GetProgramParameterName.LinkStatus, out ShaderCompileStatus);
            GL.GetProgram(ShaderProgramID, GetProgramParameterName.InfoLogLength, out ShaderCompileLogLength);
            GL.GetProgramInfoLog((int)ShaderProgramID, out ShaderCompileLog);
            if (ShaderCompileLog != null && ShaderCompileLog.Length > 0) { Console.WriteLine("Shader linking errors: {0}", ShaderCompileLog); }
            else { Console.WriteLine("Shader programs linked."); }

            GL.DeleteShader(VertexShaderID);
            GL.DeleteShader(FragmentShaderID);

            return ShaderProgramID;
        }
    }
}
