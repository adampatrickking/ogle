using OpenTK;
using OpenTK.Input;
using System.Collections.Generic;

namespace Ogle
{
	public static class Input
	{
		#region Private properties

		private static GameWindow Window { get; set; }

		private static Vector2 _MousePos { get; set; }
		private static Vector2 _MousePosLast { get; set; }

		private static List<Key> _KeysDown { get; set; }
		private static List<Key> _KeysDownLast { get; set; }

		private static List<MouseButton> _MouseButtonsDown { get; set; }
		private static List<MouseButton> _MouseButtonsDownLast { get; set; }

		private static bool _MouseLocked { get; set; }

		#endregion Private properties

		public static Vector2 MouseDelta { get; private set; }
		public static float MouseScrollDelta { get; private set; }
		public static float MouseSensitivity { get; private set; }

		/// <summary>
		/// Set key and button lists, initializes events.
		/// </summary>
		/// <param name="GameSource">Game source.</param>
		public static void Initialize(ref GameWindow WindowSource)
		{
			Window = WindowSource;

			_MousePos = new Vector2();
			_MousePosLast = new Vector2();
			_KeysDown = new List<Key>();
			_KeysDownLast = new List<Key>();
			_MouseButtonsDown = new List<MouseButton>();
			_MouseButtonsDownLast = new List<MouseButton>();

			MouseSensitivity = 2.5f;

			Window.KeyDown += MainGameWindow_KeyDown;
			Window.KeyUp += MainGameWindow_KeyUp;
			Window.MouseMove += MainGameWindow_MouseMove;
			Window.MouseUp += MainGameWindow_MouseUp;
			Window.MouseDown += MainGameWindow_MouseDown;
			Window.MouseWheel += Window_MouseWheel;
		}

		#region private event callbacks

		/// <summary>
		/// MouseMove event listener.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		private static void MainGameWindow_MouseMove(object sender, MouseMoveEventArgs e)
		{
			_MousePos = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
		}

		/// <summary>
		/// Scroll wheel event listener.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		private static void Window_MouseWheel(object sender, MouseWheelEventArgs e)
		{
			MouseScrollDelta = e.DeltaPrecise;
		}

		/// <summary>
		/// KeyUp event listener.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		private static void MainGameWindow_KeyUp(object sender, KeyboardKeyEventArgs e)
		{
			while (_KeysDown.Contains(e.Key))
				_KeysDown.Remove(e.Key);
		}

		/// <summary>
		/// KeyDown event listener.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		private static void MainGameWindow_KeyDown(object sender, KeyboardKeyEventArgs e)
		{
			_KeysDown.Add(e.Key);
		}

		/// <summary>
		/// MouseUp event listener.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		private static void MainGameWindow_MouseUp(object sender, MouseButtonEventArgs e)
		{
			while (_MouseButtonsDown.Contains(e.Button))
				_MouseButtonsDown.Remove(e.Button);
		}

		/// <summary>
		/// MouseDown event listener.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		private static void MainGameWindow_MouseDown(object sender, MouseButtonEventArgs e)
		{
			_MouseButtonsDown.Add(e.Button);
		}

		#endregion private event callbacks

		#region Public accessors

		/// <summary>
		/// Returns the mouse position.
		/// </summary>
		/// <returns>The position of the mouse as a Vector2</returns>
		public static Vector2 MousePosition()
		{
			return _MousePos;
		}

		/// <summary>
		/// Returns true for a single key press.
		/// </summary>
		/// <returns><c>true</c>, if key was pressed, <c>false</c> otherwise.</returns>
		/// <param name="key">Key.</param>
		public static bool KeyPress(Key key)
		{
			return (_KeysDown.Contains(key) && !_KeysDownLast.Contains(key));
		}

		/// <summary>
		/// Returns true if key was released.
		/// </summary>
		/// <returns><c>true</c>, if key was released, <c>false</c> otherwise.</returns>
		/// <param name="key">Key.</param>
		public static bool KeyRelease(Key key)
		{
			return (!_KeysDown.Contains(key) && _KeysDownLast.Contains(key));
		}

		/// <summary>
		/// Returns true if key is down.
		/// </summary>
		/// <returns><c>true</c>, if key is down, <c>false</c> otherwise.</returns>
		/// <param name="key">Key.</param>
		public static bool KeyDown(Key key)
		{
			return (_KeysDown.Contains(key));
		}

		/// <summary>
		/// Returns true for a single button press.
		/// </summary>
		/// <returns><c>true</c>, if mouse button was pressed, <c>false</c> otherwise.</returns>
		/// <param name="mouseButton">Mouse button.</param>
		public static bool MouseButtonPress(MouseButton mouseButton)
		{
			return (_MouseButtonsDown.Contains(mouseButton) && !_MouseButtonsDownLast.Contains(mouseButton));
		}

		/// <summary>
		/// Returns true if button was released.
		/// </summary>
		/// <returns><c>true</c>, if button was released, <c>false</c> otherwise.</returns>
		/// <param name="mouseButton">Mouse button.</param>
		public static bool MouseButtonRelease(MouseButton mouseButton)
		{
			return (!_MouseButtonsDown.Contains(mouseButton) && _MouseButtonsDownLast.Contains(mouseButton));
		}

		/// <summary>
		/// Returns true if button is down.
		/// </summary>
		/// <returns><c>true</c>, if button is down, <c>false</c> otherwise.</returns>
		/// <param name="mouseButton">Mouse button.</param>
		public static bool MouseButtonDown(MouseButton mouseButton)
		{
			return (_MouseButtonsDown.Contains(mouseButton));
		}

		/// <summary>
		/// Returns a normalized vector for mouse directional dragging.
		/// </summary>
		/// <returns><c>Vector2</c> of normalized mouse drag direction.</returns>
		public static Vector2 MouseDrag()
		{
			float newX, newY;

			if (_MousePos != _MousePosLast)
			{
				if (_MousePos.X != _MousePosLast.X)
					newX = (_MousePos.X > _MousePosLast.X) ? 0.001f : -0.001f;
				else newX = 0f;

				if (_MousePos.Y != _MousePosLast.Y)
					newY = (_MousePos.Y > _MousePosLast.Y) ? 0.001f : -0.001f;
				else newY = 0f;

				return new Vector2(newX, newY);
			}

			return _MousePos;
		}

		/// <summary>
		/// Returns true if the mouse previous is not equal to the
		/// current mouse position on either X or Y.
		/// </summary>
		/// <returns></returns>
		public static bool MouseMoving()
		{
			return (_MousePos != _MousePosLast);
		}

		/// <summary>
		/// A call to this function will lock/unlock the cursor from
		/// the centre of the screen.
		/// </summary>
		/// <param name="LockState"></param>
		public static void LockMouseCentre(bool LockState)
		{
			_MouseLocked = LockState;

			if (_MouseLocked) ResetMousePosition();
		}

		#endregion Public accessors

		/// <summary>
		/// Resets the mouse position to the centre of the window and
		/// sets the mouse delta position as a result before reset.
		/// </summary>
		private static void ResetMousePosition()
		{
			if (Game.Window.Focused)
			{
				MouseDelta = (_MousePos - _MousePosLast) * MouseSensitivity;
				Mouse.SetPosition(Game.WindowCentre.X, Game.WindowCentre.Y);
			}
		}

		/// <summary>
		/// Updates all key and button lists.
		/// </summary>
		public static void Update()
		{
			LockMouseCentre(_MouseLocked);

			_MousePosLast = _MousePos;
			_KeysDownLast = new List<Key>(_KeysDown);
			_MouseButtonsDownLast = new List<MouseButton>(_MouseButtonsDown);
			MouseScrollDelta = 0f;
		}
	}
}