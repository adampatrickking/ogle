using Ogle.Shaders;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using System;
using System.Drawing;

namespace Ogle
{
	public static class Game
	{
		public static GameWindow Window;
		public static Vector2 WindowCentre { get; private set; }
		public static float AspectRatio { get; set; }
        private static int TotalFrames { get; set; }
        private static double PreviousTime { get; set; }
		public static double DeltaTime { get; set; }
		public static uint ShaderProgram { get; set; }
		private static FirstPersonCamera GameCamera { get; set; }

		/// <summary>
		/// Set-up the events for the Window
		/// </summary>
		/// <param name="_Window"></param>
		public static void Initialize(ref GameWindow _Window)
		{
			Window = _Window;

			Window.Load += Window_Load;
			Window.Resize += Window_Resize;
			Window.UpdateFrame += Window_UpdateFrame;
			Window.RenderFrame += Window_RenderFrame;

			Input.Initialize(ref Window);
		}

		private static void Window_Load(object sender, EventArgs e)
		{
			Window.WindowState = WindowState.Normal;

			// Lock the mouse cursor to the centre of the screen
			Input.LockMouseCentre(true);
			Window.CursorVisible = false;

            TotalFrames = 0;

			// Create a shader program
			ShaderProgram = ShaderUtils.CreateProgramFromFiles("VertexShader", "FragmentShader");
			GL.UseProgram(ShaderProgram);

			// Define the client aspect ration on load so the camera
			// may use it before Resize is called.
			AspectRatio = (float)Window.Width / (float)Window.Height;

			// Create a camera once the matrices are located
            GameCamera = new FirstPersonCamera(new Vector3(0f, 0f, -5f));

			// Enable depth buffering
			GL.Enable(EnableCap.DepthTest);
			GL.DepthFunc(DepthFunction.Less);

			// Enable front facial triangle culling
            GL.Enable(EnableCap.CullFace);
            GL.CullFace(CullFaceMode.Back);

			GL.Enable(EnableCap.LineSmooth);

            Drawing.Init();
		}

		private static void Window_Resize(object sender, EventArgs e)
		{
			// Define the client aspect ration on resize
			AspectRatio = (float)Window.Width / (float)Window.Height;

			WindowCentre = new Vector2((Game.Window.X + Game.Window.Width / 2), (Game.Window.Y + Game.Window.Height / 2));

			// Define the client viewport on load and resize
			GL.Viewport(Window.ClientRectangle);
		}

		private static void Window_UpdateFrame(object sender, FrameEventArgs e)
		{
			GL.ClearColor(Color.WhiteSmoke);

            if (Input.KeyDown(Key.W)) GameCamera.TranslateForwardsBackwards(3f * (float)DeltaTime);
            if (Input.KeyDown(Key.A)) GameCamera.TranslateLeftRight(3f * (float)DeltaTime);
            if (Input.KeyDown(Key.S)) GameCamera.TranslateForwardsBackwards(-3f * (float)DeltaTime);
            if (Input.KeyDown(Key.D)) GameCamera.TranslateLeftRight(-3f * (float)DeltaTime);

			// Close upon escape
			if (Input.KeyPress(Key.Escape)) Window.Close();
			
			GameCamera.Update();                       

			Input.Update();
		}

		private static void Window_RenderFrame(object sender, FrameEventArgs e)
		{
            if (Window.RenderFrequency <= 30)
                Console.ForegroundColor = ConsoleColor.Red;
            else if (Window.RenderFrequency >= 60)
                Console.ForegroundColor = ConsoleColor.Cyan;
            else
                Console.ForegroundColor = ConsoleColor.Green;

            Console.WriteLine(Window.RenderFrequency);

			DeltaTime = e.Time;

			GL.Clear(ClearBufferMask.DepthBufferBit | ClearBufferMask.ColorBufferBit);

			Drawing.Draw();

			Window.SwapBuffers();
		}
	}
}