using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Ogle
{
	public static class BufferFactory
	{
		public static uint[] CreateBuffers(float[] VertexArray, uint[] IndexArray, float[] ColorArray = null, float[] TextureArray = null, uint[] NormalArray = null)
		{
			uint BufferHandle = 0;

			#region fields
			uint VertexBuffer = 0;
			uint IndexBuffer = 0;
			uint ColorBuffer = 0;
			uint TextureBuffer = 0;
			uint NormalBuffer = 0;
			#endregion

			// Create and set VAO & handle
			GL.GenVertexArrays(1, out BufferHandle);
			GL.BindVertexArray(BufferHandle);            

			// Create and set VBO & handle (Vertices)
			if (VertexArray != null && VertexArray.Length > 0)
			{
				GL.GenBuffers(1, out VertexBuffer);
				GL.BindBuffer(BufferTarget.ArrayBuffer, VertexBuffer);
				GL.BufferData<float>(BufferTarget.ArrayBuffer, (IntPtr)(sizeof(float) * VertexArray.Length), VertexArray, BufferUsageHint.StaticDraw);
				GL.VertexPointer(3, VertexPointerType.Float, 0, 0);
				// Create pointer for 0 index (VertexPosition)
				GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 0, 0);
			}
			else
			{

			}

			// Create and set VBO & handle (Indices)
			//if (IndexArray != null && IndexArray.Length > 0)
			//{
			//GL.GenBuffers(1, out IndexBuffer);
			//GL.BindBuffer(BufferTarget.ElementArrayBuffer, IndexBuffer);
			//GL.BufferData<uint>(BufferTarget.ElementArrayBuffer, (IntPtr)(sizeof(uint) * IndexArray.Length), IndexArray, BufferUsageHint.StaticDraw);
			//}
			//else {

			//}

			// Create and set VBO & handle (Colors)
			if (ColorArray != null && ColorArray.Length > 0)
			{
				GL.GenBuffers(1, out ColorBuffer);
				GL.BindBuffer(BufferTarget.ArrayBuffer, ColorBuffer);
				GL.BufferData<float>(BufferTarget.ArrayBuffer, (IntPtr)(sizeof(float) * ColorArray.Length), ColorArray, BufferUsageHint.StaticDraw);
				// Create pointer for 1 index (VertexColor)
				GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, false, 0, 0);
			}
			else
			{

			}

			// Create and set VBO & handle (Textures)
			// Create and set VBO & handle (Normals)

			// Unbind all VAOs and VBOs
			GL.BindVertexArray(0);
			GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);

			return new uint[] {
				BufferHandle,
				VertexBuffer,
				IndexBuffer,
				TextureBuffer,
				NormalBuffer
			};
		}
	}
}
