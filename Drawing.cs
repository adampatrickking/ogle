using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Input;

using OpenTK.Graphics.OpenGL;

namespace Ogle
{
	public static class Drawing
	{
		// Define a debug mesh and apply the same mesh to two GameObjects.
		private static Mesh MeshSuzanne = MeshFactory.CreateFromFile("Suzanne");

        private static List<GameObject> Objects = new List<GameObject>();
        private static Random Rand = new Random();

		/// <summary>
		/// A draw function called from within OnRenderFrame event and
		/// is such within a restrained update rate (frame buffer swap speed).
		/// </summary>
		public static void Draw()
		{
            Console.WriteLine(Vector3.SizeInBytes * 1000);
            foreach (GameObject Obj in Objects)
            {
                // Obj.Rotate(new Vector3((float)Rand.NextDouble() / 10, (float)Rand.NextDouble() / 10, (float)Rand.NextDouble() / 10));
                Obj.Draw();
            }
		}

        public static void Init()
        {
            Vector3 Location = Vector3.Zero;

            for (int i = 0; i < 1000; i++)
            {
                GameObject Obj = new GameObject(MeshSuzanne);
                Obj.Reposition(Location);
                Obj.Orientate(new Vector3(0f, 3f, 0f));
                Objects.Add(Obj);

                if (Location.X < 100f)
                {
                    Location += new Vector3(3f, 0f, 0f);
                }
                if (Location.X > 100f)
                {
                    Location.Z += 3f;
                    Location.X = 0f;
                }

            }
        }
	}
}