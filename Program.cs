using OpenTK;

namespace Ogle
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			GameWindow Window = new GameWindow(1440, 900, new OpenTK.Graphics.GraphicsMode(32, 24, 0, 64));
			Game.Initialize(ref Window);
			Window.Run();
		}
	}
}